function K= move_me(A)
f=input('f=');
K = f*ones(size(A));
t=A~=f; 
K(6-sum(t):end) = A(t);
end
