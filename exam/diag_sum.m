function s=diag_sum(A)
[u,h]=size(A);
t=min(u,h);
i=(t+1)/2;
A=A([1:t],[1:t]);
B=flip(A);
if mod(t,2)==0
     s=sum(diag(A))+sum(diag(B));
else
     s=sum(diag(A))+sum(diag(B))-A(i,i);
end
end